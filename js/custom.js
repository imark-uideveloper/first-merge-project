(function($) {
  'use strict';

  //Testimonials
  var partnerSlider = $('#partnerSlider');
  if (partnerSlider.length > 0) {
    partnerSlider.owlCarousel({
      loop: false,
      margin: 0,
      items: 4,
      nav: false,
      dots: false
    })
  };

  //Similar Product
  var partnerSlider = $('#SimilarProductSlider');
  if (partnerSlider.length > 0) {
    partnerSlider.owlCarousel({
      loop: false,
      margin: 15,
      items: 3,
      nav: true,
      dots: false
    })
  };

  //Similar Gift Card Slider
  var partnerSlider = $('#SimilarGiftCardSlider');
  if (partnerSlider.length > 0) {
    partnerSlider.owlCarousel({
      loop: false,
      margin: 0,
      items: 5,
      nav: true,
      dots: false
    })
  };

  //Date Picker
  // $('[data-toggle="datepicker"]').datepicker({
  //   inline:true,
  //         zIndex:9999
  //   });

  //Product Slider Flex
  $(window).load(function() {
    // The slider being synced must be initialized first
    $('#product-carousel').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemWidth: 125,
      itemMargin: 23,
      asNavFor: '#product-slider'
    });

    $('#product-slider').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      sync: "#product-carousel"
    });
  });


  //Product Slider Flex
  $(window).load(function() {
    // The slider being synced must be initialized first
    $('#gift-carousel').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemWidth: 125,
      itemMargin: 23,
      asNavFor: '#gift-slider'
    });

    $('#gift-slider').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      sync: "#gift-carousel"
    });
  });


  // Close Search
  var searchCover = $('.search-nearby-form');
  var searchBtn = $('.close-search-nearby');

  searchBtn.click(function() {
    searchCover.toggleClass("closeBar");
  })

  //Dashboard Scrollbar

  $(function() {
    $(window).on("load", function() {
      $(".dashboard aside").mCustomScrollbar({
        axis: "y"
      });
    });
  });

  //Custom Model

  $(function() {
    //----- OPEN
    $('[pd-popup-open]').on('click', function(e) {
      var targeted_popup_class = jQuery(this).attr('pd-popup-open');
      $('[pd-popup="' + targeted_popup_class + '"]').fadeIn(100);

      e.preventDefault();
    });

    //----- CLOSE
    $('[pd-popup-close]').on('click', function(e) {
      var targeted_popup_class = jQuery(this).attr('pd-popup-close');
      $('[pd-popup="' + targeted_popup_class + '"]').fadeOut(200);

      e.preventDefault();
    });
  });

  //Upload images
  $(function() {
    $(".user-img input:file").on('change', function() {
      var fileName = $(this).val();
      if (fileName.length > 0) {
        $(this).parent().children('span').html(fileName);
      } else {
        $(this).parent().children('span').html("Choose file");
      }
    });
    //file input preview
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('.user-img img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }
    $('body').find(".user-img input[type='file']").on('change', function() {
      readURL(this);
    });
  });


  //On Scroll add class

  $(function() {
    //caches a jQuery object containing the header element
    var header = $(".dashboard header");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 5) {
            header.addClass("scrollHeader");
        } else {
            header.removeClass("scrollHeader");
        }
    });
});

  //Avoid pinch zoom on iOS
  document.addEventListener('touchmove', function(event) {
    if (event.scale !== 1) {
      event.preventDefault();
    }
  }, false);
})(jQuery)
